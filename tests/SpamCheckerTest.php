<?php

namespace App\Tests;

use App\Entity\Comment;
use App\SpamChecker;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\HttpClient\ResponseInterface;

class SpamCheckerTest extends TestCase
{
    public function testSpamScoreWithInvalidRequest(): void
    {
        $comment = new Comment();
        $comment->setCreatedAtValue();
        $context = [];

        $client = new MockHttpClient([
            new MockResponse('invalid', [
                'response_headers' => ['x-akismet-debug-help: Invalid key'],
            ]),
        ]);

        $spamChecker = new SpamChecker($client, 'abcdef');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage(
            'Unable to check for spam: invalid (Invalid key).'
        );
        $spamChecker->getSpamScore($comment, $context);
    }

    /**
     * @dataProvider getComments
     */
    public function testSpamScore(
        int $expectedScore,
        ResponseInterface $response,
        Comment $comment,
        array $context
    ): void {
        $client = new MockHttpClient([$response]);
        $spamChecker = new SpamChecker($client, 'abcdef');
        $score = $spamChecker->getSpamScore($comment, $context);
        $this->assertSame($expectedScore, $score);
    }

    public function getComments(): iterable
    {
        $comment = new Comment();
        $comment->setCreatedAtValue();
        $context = [];

        yield 'ham' => [0, new MockResponse('false'), $comment, $context];
        yield 'spam' => [1, new MockResponse('true'), $comment, $context];
        yield 'blatant_spam' => [
            2,
            new MockResponse('', [
                'response_headers' => ['x-akismet-pro-tip: discard'],
            ]),
            $comment,
            $context,
        ];
    }
}
