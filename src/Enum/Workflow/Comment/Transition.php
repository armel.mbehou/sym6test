<?php

namespace App\Enum\Workflow\Comment;


enum Transition: string
{
    # First transition executed when SpamChecker return 0 (trusted comment)
    case Accept = 'accept';

    # Executed when SpamChecker return 1 (Need human review)
    case MightBeSpam = 'might_be_spam';

    # Executed when SpamChecker service return 2 (Spam)
    case RejectSpam = 'rejected_spam';

    # Executed after user Action : Publish or Reject Ham or Potential spam
    case Reject = 'reject';
    case RejectHam = 'reject_ham';
    case Publish = 'publish';
    case PublishHam = 'publish_ham';

    # Executed if the comment is ready to publish
    case Optimize = 'optimize';
}
