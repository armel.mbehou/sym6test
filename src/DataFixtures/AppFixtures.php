<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\Comment;
use App\Entity\Conference;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

class AppFixtures extends Fixture
{
    private PasswordHasherFactoryInterface $passwordHasherFactory;

    public function __construct(
        PasswordHasherFactoryInterface $passwordHasherFactory
    ) {
        $this->passwordHasherFactory = $passwordHasherFactory;
    }

    public function load(ObjectManager $manager): void
    {
        $amsterdam = new Conference();
        $amsterdam->setCity('Amsterdam');
        $amsterdam->setYear(2019);
        $amsterdam->setIsInternational(true);
        $manager->persist($amsterdam);

        $paris = new Conference();
        $paris->setCity('Paris');
        $paris->setYear(2022);
        $paris->setIsInternational(false);
        $manager->persist($paris);

        $comment = new Comment();
        $comment->setAuthor('John Doe');
        $comment->setText(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        );
        $comment->setState('published');
        $comment->setConference($amsterdam);
        $comment->setEmail('john.doe@example.com');
        $manager->persist($comment);

        $comment1 = new Comment();
        $comment1->setAuthor('Jane Doe');
        $comment1->setText(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        );
        $comment1->setState('published');
        $comment1->setConference($amsterdam);
        $comment1->setEmail('jane.doe@example.com');
        $manager->persist($comment1);

        $admin = new Admin();
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setUsername('admin');
        $admin->setPassword(
            $this->passwordHasherFactory
                ->getPasswordHasher(Admin::class)
                ->hash('admin', null)
        );
        $manager->persist($admin);

        $manager->flush();
    }
}
