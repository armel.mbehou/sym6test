<?php

namespace App\Message;

final class CommentMessage
{

    public function __construct(
        private int $id,
        private $reviewUrl,
        private array $context)
    {
        $this->id = $id;
        $this->reviewUrl = $reviewUrl;
        $this->context = $context;
    }

    public function getId(): int
    {
        return $this->id;
    }
    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * Get the value of reviewUrl
     */
    public function getReviewUrl()
    {
            return $this->reviewUrl;
    }
}
