<?php

namespace App;

use App\Entity\Comment;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SpamChecker
{
    private HttpClientInterface $client;
    private string $endpoint;

    public function __construct(HttpClientInterface $client, string $akismetKey)
    {
        $this->client = $client;
        $this->endpoint = sprintf(
            'https://%s.rest.akismet.com/1.1/comment-check',
            $akismetKey
        );
    }

    public function getSpamScore(Comment $comment, array $context): int
    {
        $response = $this->client->request('POST', $this->endpoint, [
            'body' => array_merge($context, [
                'blog' => 'https://localhost:8000/',
                'comment_type' => 'comment',
                'comment_author' => $comment->getAuthor(),
                'comment_content' => $comment->getText(),
                'comment_author_email' => $comment->getEmail(),
                'comment_post_modified' => $comment
                    ->getCreatedAt()
                    ->format('c'),
                'blog_lang' => 'en',
                'blog_charset' => 'UTF-8',
                'is_test' => true,
            ]),
        ]);
        $header = $response->getHeaders();
        if ('discard' === ($header['x-akismet-pro-tip'][0] ?? '')) {
            return 2;
        }

        $content = $response->getContent();
        if (isset($header['x-akismet-debug-help'][0])) {
            throw new \RuntimeException(
                sprintf(
                    'Unable to check for spam: %s (%s).',
                    $content,
                    $header['x-akismet-debug-help'][0]
                )
            );
        }
        return 'true' === $content ? 1 : 0;
    }
}
