<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Enum\Workflow\Comment\Transition;
use App\Message\CommentMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpCache\StoreInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Workflow\Registry;
use Twig\Environment;

#[Route('/admin')]
class AdminController extends AbstractController
{
    public function __construct(
        private Environment $twig,
        private EntityManagerInterface $em,
        private MessageBusInterface $bus
    ) {
        $this->twig = $twig;
        $this->em = $em;
        $this->bus = $bus;
    }

    #[Route('/comment/review/{id}', name: 'review_comment')]
    public function index(
        Request $request,
        Comment $comment,
        Registry $registry
    ): Response {
        $accept = !$request->get('reject', false);
        dump($accept);
        $machine = $registry->get($comment);
        if ($machine->can($comment, Transition::Publish->value)) {
            $transition = $accept
            ? Transition::Publish->value
            : Transition::Reject->value;
        } elseif ($machine->can($comment, Transition::PublishHam->value)) {
            $transition = $accept
            ? Transition::PublishHam->value
            : Transition::RejectHam->value;
        } else {
            return new Response(
                'Comment already reviewed or not in the right state'
            );
        }
        dump($transition);
        $machine->apply($comment, $transition);
        $this->em->flush();

        if ($accept) {
            $reviewUrl = $this->generateUrl('review_comment', ['id' => $comment->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
            $this->bus->dispatch(new CommentMessage($comment->getId(), $reviewUrl, []));
        }

        return $this->render('admin/review.html.twig', [
            'transition' => $transition,
            'comment' => $comment,
        ]);
    }

    #[Route('/http_cache/{uri<.*>}', name: 'purge_cahce', methods: ['PURGE'])]
    public function purgeHttpCache(KernelInterface $kernel, Request $request, string $uri, StoreInterface $store): Response
    {
        if ('prod' === $kernel->getEnvironment()) {
            return new Response('KO', Response::HTTP_BAD_REQUEST);
        }
        $store->purge($request->getScheme().'/'.$uri);
        return new Response('Done', Response::HTTP_OK);
    }
}
