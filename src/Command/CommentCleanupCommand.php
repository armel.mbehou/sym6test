<?php

namespace App\Command;

use App\Repository\CommentRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:comment:cleanup',
    description: 'Add a short description for your command',
)]
class CommentCleanupCommand extends Command
{
    private CommentRepository $cr;

    public function __construct(CommentRepository $cr)
    {
        $this->cr = $cr;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Deletes rejeted and spam comments from database')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getOption('dry-run')) {
            $io->note('Dry run enabled');
            $count = $this->cr->countOldRejected();
        } else {
            $count = $this->cr->deleteOldRejected();
        }

        $io->success(sprintf('Deleted %d Rejected/Spam comments.', $count));

        return Command::SUCCESS;
    }
}
