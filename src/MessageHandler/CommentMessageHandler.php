<?php

namespace App\MessageHandler;

use App\SpamChecker;
use App\Entity\Comment;
use App\Enum\Workflow\Comment\Transition;
use App\ImageOptimizer;
use Psr\Log\LoggerInterface;
use App\Message\CommentMessage;
use App\Notification\CommentReviewNotification;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Workflow\WorkflowInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Notifier\NotifierInterface;

final class CommentMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private SpamChecker $spamChecker,
        private EntityManagerInterface $entityManager,
        private CommentRepository $commentRepository,
        private MessageBusInterface $bus,
        private WorkflowInterface $commentStateMachine,
        private ImageOptimizer $imageOptimizer,
        private NotifierInterface $notifier,
        private string $photoDir,
        private LoggerInterface $logger
    ) {
        $this->spamChecker = $spamChecker;
        $this->entityManager = $entityManager;
        $this->commentRepository = $commentRepository;
        $this->bus = $bus;
        $this->workflow = $commentStateMachine;
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->imageOptimizer = $imageOptimizer;
        $this->photoDir = $photoDir;
    }

    public function __invoke(CommentMessage $message)
    {
        $comment = $this->commentRepository->find($message->getId());
        if (!$comment) {
            return;
        }

        if ($this->workflow->can($comment, Transition::Accept->value)) {
            $transition = $this->getTransitionFromSpamScore($comment, $message);

            $this->workflow->apply($comment, $transition);
            $this->entityManager->flush();
            $this->bus->dispatch($message);
        } elseif (
            $this->workflow->can($comment, Transition::Publish->value) ||
            $this->workflow->can($comment, Transition::PublishHam->value)
        ) {
            $this->sendNotification($comment, $message->getReviewUrl());
        } elseif ($this->workflow->can($comment, Transition::Optimize->value)) {
            if ($comment->getPhotoFilename()) {
                $this->imageOptimizer->resize(
                    $this->photoDir . '/' . $comment->getPhotoFilename()
                );
            }
            $this->workflow->apply($comment, Transition::Optimize->value);
            $this->entityManager->flush();
        } else {
            $this->logger->debug('Dropping comment message #{id}.', [
                'comment' => $comment->getId(),
                'state' => $comment->getState(),
            ]);
        }
    }

    private function getTransitionFromSpamScore(
        Comment $comment,
        CommentMessage $message
    ): string {
        $score = $this->spamChecker->getSpamScore(
            $comment,
            $message->getContext()
        );
        $transition = Transition::Accept;
        if (2 === $score) {
            $transition = Transition::RejectSpam;
        } elseif (1 === $score) {
            $transition = Transition::MightBeSpam;
        }

        return $transition->value;
    }

    private function sendNotification($comment, $reviewUrl)
    {
        $this->notifier->send(
            new CommentReviewNotification($comment, $reviewUrl),
            ...$this->notifier->getAdminRecipients());

    }
}
