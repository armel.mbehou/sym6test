<?php

return [
    'AKISMET_KEY' => null,
    'SLACK_ACCESS_TOKEN' => null,
    'SLACK_CHANNEL' => null,
];
