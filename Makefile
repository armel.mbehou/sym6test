SHELL := /bin/bash

.PHONY: tests

tests:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test
	symfony console doctrine:fixtures:load -n --env=test
	symfony php bin/phpunit --testdox $@

gitpush:
	git add .
	git commit -m "$m"
	git push

start-project:
###net start com.docker.service
	docker-compose up -d
	symfony server:start -d
	symfony open:local
	symfony open:local:webmail
	symfony run -d --watch=config,src,vendor,template symfony console messenger:consume async
	symfony run -d yarn dev --watch
	symfony server:log

stop-project:
	symfony server:stop
	docker-compose down
###net stop com.docker.service

dump-workflow:
	symfony console workflow:dump $(name) | dot -Tpng -o workflow.png

#symfony console messenger:failed:show --max 10
#symfony console messenger:failed:retry

clear-cache:
#curl -s -I -X PURGE -u admin:admin `symfony var:export SYMFONY_PROJECT_DEFAULT_ROUTE_URL`/admin/http-cache/
	curl -s -I -X PURGE -u admin:admin `symfony var:export SYMFONY_PROJECT_DEFAULT_ROUTE_URL`/admin/http-cache/conference_header